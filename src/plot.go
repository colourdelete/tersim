package main

type plottable struct {
	grid       [][]float64 // data
	N          int         // width
	M          int         // height
	resolution float64     // resolution (eg 0.5)
	minX       float64     // physical coordinates the 0, 0 cell should assume
	minY       float64     // physical coordinates the 0, 0 cell should assume
}

func (p plottable) Dims() (c, r int) {
	return p.N, p.M
}
func (p plottable) Y(r int) float64 {
	return p.minY + float64(r)*p.resolution
}
func (p plottable) Z(c, r int) float64 {
	return p.grid[c][r]
}
