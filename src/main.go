package main

import (
	"fmt"
	"math/rand"
	"os"
	"sync"
)

func panicAtError(e error) {
	if e != nil {
		panic(e)
	}
}

func calcSvgPart(planet0 Body, x, y int, returnChan chan string) {
	var hue int = int(planet0.mesh["terrain"].content[x][y].height * 255)
	if hue > 255 {
		hue = 255
	}
	returnChan <- fmt.Sprintf("<rect width=\"1\" height=\"1\" x=\"%v\" y=\"%v\" fill=\"rgb(%v, %v, %v)\"/>", x, y, hue, hue, hue)
}

func pressureMeshGen(planet0 Body, pressureMesh *MeshPlane, wg *sync.WaitGroup) {
	pressureMesh.content[rand.Intn(planet0.mesh["terrain"].xSize)][rand.Intn(planet0.mesh["terrain"].ySize)].height = rand.Float64()
	wg.Done()
}

func main() {
	fmt.Println("TerSim by Colourdelete on GitLab.com.")
	planet0 := newBodyWithSize("planet", 200, 200)
	pressureMesh := newMeshPlaneBare(planet0.mesh["terrain"].xSize, planet0.mesh["terrain"].ySize)
	step1Wg := new(sync.WaitGroup)
	maxI := rand.Intn(planet0.mesh["terrain"].xSize * planet0.mesh["terrain"].ySize)
	step1Wg.Add(maxI)
	var step1Wg_ *sync.WaitGroup = step1Wg
	for i := 0; i < maxI; i++ {
		//pressureMesh.content[rand.Intn(planet0.mesh["terrain"].xSize)][rand.Intn(planet0.mesh["terrain"].ySize)].height = rand.Float64()
		go pressureMeshGen(planet0, &pressureMesh, step1Wg_)
	}
	step1Wg.Wait()
	fmt.Println("Step 1 Done")
	planet0.typeHandler("terrain", 60, pressureMesh)
	var svgContent = ""
	var returnChan = make(chan string, planet0.mesh["terrain"].xSize*planet0.mesh["terrain"].ySize)
	for x := 0; x < planet0.mesh["terrain"].xSize; x++ {
		for y := 0; y < planet0.mesh["terrain"].ySize; y++ {
			go calcSvgPart(planet0, x, y, returnChan)
			//hue = int(planet0.mesh["terrain"].content[x][y].height * 255)
			//if hue > 255 {
			//	hue = 255
			//}
			//svgContent += fmt.Sprintf("<rect width=\"1\" height=\"1\" x=\"%v\" y=\"%v\" fill=\"hsl(%v, %s, %s)\"/>", x, y, hue, "100%", "50%")
		}
	}
	for i := 0; i < planet0.mesh["terrain"].xSize*planet0.mesh["terrain"].ySize; i++ {
		svgContent += <-returnChan
	}
	fmt.Println("Step 2 Done")
	var svg = fmt.Sprintf("<svg viewBox=\"0 0 %v %v\" xmlns=\"http://www.w3.org/2000/svg\"><rect width=\"%v\" height=\"%v\" x=\"0\" y=\"0\" fill=\"rgb(0, 0, 0)\"/>%s</svg>", planet0.mesh["terrain"].xSize, planet0.mesh["terrain"].ySize, planet0.mesh["terrain"].xSize, planet0.mesh["terrain"].ySize, svgContent)
	fo, err := os.Create("terrain.svg")
	if err != nil {
		panic(err)
	}
	// close fo on exit and check for its returned error
	defer func() {
		if err := fo.Close(); err != nil {
			panic(err)
		}
	}()

	// write a chunk
	if _, err := fo.Write([]byte(svg)); err != nil {
		panic(err)
	}
}
