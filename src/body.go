package main

import (
	"fmt"
	"math"
)

type Body struct {
	age  float64
	mesh map[string]MeshPlane
}

func (body Body) show() {
	fmt.Println("show")
	fmt.Println("Content of body")
	var xSize, ySize int
	for key, mesh := range body.mesh {
		fmt.Printf("MeshPlane %s\n", key)
		xSize = mesh.xSize
		ySize = mesh.ySize
		for x := 0; x < xSize; x++ {
			for y := 0; y < ySize; y++ {
				fmt.Printf("%v\t", math.Round(mesh.content[x][y].height*100000)/100000)
			}
			fmt.Println()
		}
	}
}

func (body Body) typeHandler(type_ string, time float64, pressureMesh MeshPlane) {
	fmt.Println("typeHandler")
	var mesh MeshPlane
	mesh = body.mesh[type_]
	if type_ == "terrain" {
		mesh.handler(mesh, pressureMesh)
	} else if type_ == "water" {
		mesh.handler(mesh, pressureMesh)
	}
}

func newBody(type_ string) Body {
	return newBodyWithSize(type_, 1, 1)
}

func newBodyWithSize(type_ string, xSize, ySize int) Body {
	var body Body
	var age float64
	var mesh map[string]MeshPlane
	mesh = make(map[string]MeshPlane)
	if type_ == "planet" {
		age = 0.0
		mesh["terrain"] = newMeshPlane(xSize, ySize, getMatchingMeshPlaneHandler("terrain"), cosAdhesiveFunc)
		mesh["water"] = newMeshPlane(xSize, ySize, getMatchingMeshPlaneHandler("water"), cosAdhesiveFunc)
		body = Body{age, mesh}
	}
	return body
}
