package main

import (
	"math"
)

type MeshPlane struct {
	handler        func(mesh MeshPlane, pressureMesh MeshPlane) MeshPlane
	xSize          int
	ySize          int
	content        [][]MeshPoint
	adhesiveFunc   func(length float64) float64
	adhesiveRadius int
	adhesiveCoeff  float64
	adhesiveOffset float64
}

func dummyHandler(mesh MeshPlane, pressureMesh MeshPlane) MeshPlane {
	return mesh
}

func overflowAdd(a int, b int, limitA, limitB int) int {
	resultCand := (a + b) % limitB
	if resultCand < limitA {
		resultCand = limitA
	}
	return resultCand
}

func terrainHandler(mesh MeshPlane, pressureMesh MeshPlane) MeshPlane {
	draftMesh := newMeshPlane(mesh.xSize, mesh.ySize, dummyHandler, cosAdhesiveFunc)
	for x := 0; x < draftMesh.xSize; x++ {
		for y := 0; y < draftMesh.ySize; y++ {
			draftMesh.content[x][y].height += pressureMesh.content[x][y].height
		}
	}
	for x := 0; x < draftMesh.xSize; x++ {
		var dist, offset float64
		for y := 0; y < draftMesh.ySize; y++ {
			for x2 := -1 * mesh.adhesiveRadius; x2 < mesh.adhesiveRadius; x2++ {
				for y2 := -1 * mesh.adhesiveRadius; y2 < mesh.adhesiveRadius; y2++ {
					dist = math.Sqrt(math.Abs(float64(x2 ^ 2 + y2 ^ 2)))
					if dist > float64(mesh.adhesiveRadius) {
						offset = 0
					} else {
						offset = draftMesh.content[overflowAdd(x2, x, 0, draftMesh.xSize)][overflowAdd(y2, y, 0, draftMesh.ySize)].height*mesh.adhesiveFunc(dist)*mesh.adhesiveCoeff + mesh.adhesiveOffset
						//fmt.Println(dist, offset, draftMesh.content[overflowAdd(x2, x, 0, draftMesh.xSize)][overflowAdd(y2, y, 0, draftMesh.ySize)].height, mesh.adhesiveFunc(dist))
					}
					mesh.content[x][y].height += offset
				}
			}
		}
	}
	return mesh
}

func getMatchingMeshPlaneHandler(type_ string) func(mesh MeshPlane, pressureMesh MeshPlane) MeshPlane {
	if type_ == "terrain" {
		return terrainHandler
	} else {
		return dummyHandler
	}
}

func (meshPlane MeshPlane) toPlainPlane() [][]float64 {
	var plainPlane [][]float64
	plainPlane = make([][]float64, meshPlane.xSize)
	for x := 0; x < meshPlane.xSize; x++ {
		plainPlane[x] = make([]float64, meshPlane.ySize)
		for y := 0; y < meshPlane.ySize; y++ {
			plainPlane[x][y] += meshPlane.content[x][y].height
		}
	}
	return plainPlane
}

func cosAdhesiveFunc(length float64) float64 {
	resultCand := 1 - math.Sqrt(-1*(length-length)*length)
	//resultCand := 0.5*math.Cos(math.Pi*length) + 0.5
	return resultCand
	//if 0 > resultCand {
	//	return 0
	//} else if 1 < resultCand {
	//	return 1
	//} else {
	//	return resultCand
	//}
}

func linearAdhesiveFunc(length float64) float64 {
	return length
}

func newMeshPlaneBare(xSize, ySize int) MeshPlane {
	return newMeshPlane(xSize, ySize, dummyHandler, cosAdhesiveFunc)
	//return newMeshPlane(xSize, ySize, dummyHandler, linearAdhesiveFunc)
}

func newMeshPlane(xSize, ySize int, handler func(mesh MeshPlane, pressureMesh MeshPlane) MeshPlane, adhesiveFunc func(length float64) float64) MeshPlane {
	var mesh MeshPlane
	mesh = MeshPlane{
		handler:        handler,
		xSize:          xSize,
		ySize:          ySize,
		adhesiveFunc:   adhesiveFunc,
		adhesiveCoeff:  0.05,
		adhesiveOffset: 0.0,
		adhesiveRadius: 10,
		content:        make([][]MeshPoint, xSize),
	}
	for x := 0; x < xSize; x++ {
		mesh.content[x] = make([]MeshPoint, ySize)
		for y := 0; y < ySize; y++ {
			mesh.content[x][y] = MeshPoint{height: 0}
		}
	}
	return mesh
}
